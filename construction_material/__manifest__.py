#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Maduka Sopulu Chris kingston
#
# Created:     20/04/2018
# Copyright:   (c) kingston 2018
# Licence:     <your licence>
#-------------------------------------------------------------------------------
{
    'name': 'Construction BOQ Generic',
    'version': '10.0.1.0.0',
    'author': 'Maduka Sopulu',
    'description':"""MBill of quantity Application to enable QS determine the stage and activities of a project""",
    'category': 'Project',

    'depends': ['base','product','project','branch', 'web_timeline','construction_rewrite'],
    'data': [
        'security/security_group.xml',
        'security/ir.model.access.csv',
        'views/construction_material_views.xml',
    ],
    'price': 1400.99,
    'currency': 'USD',


    'installable': True,
    'auto_install': False,
}
