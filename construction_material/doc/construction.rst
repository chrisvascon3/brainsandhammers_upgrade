Average price



Construction Application:
------------------------
Before any project proceeds, you have to set the bill of quantity for each project
How to Do:
- Go to Configuration Menu:
- Create Labour and Material Payment Schedule
- Create Activities - Add the Materials and Labour Schedule require for this project
- Create the Activities Stage
- Go to Bill of Quantity and Create One for a project.
Adding the projects, branches and house types will only enable you to view the stages, activities, materials and Labour required to achieve a project.
Once the BOQ is created, the system calculates the estimated bill of quantity required to do a project.

Construction Budget
On creation of the child budgets(Building, Ancilliary, Salary, Opex and Capex), they are all linked to the master budget set for a project or period


===========================
Material Usage and Request:
---------------------------

To Use Material Request/Usage Features,
Click to Material Request/Usage Menu and Select the mode.

----------------------------------------------------------
-For Procurement Process(Select Request Procurement Mode):

Start entering details
The request is based on Budget Created for the Project (This ensures that the request doesn't pass the selected budget amount)
Select the item Lines, if the product exists in the store, the system automtically updates the product stock quantity for the project
After adding the items, Click the workflow buttons to generate a procurement request.
This generates a Purchase Order,

Each Workflow signals sends a mail notification to the respective user group

Go to Procurement -- > Purchase Orders --> You can see the PO, Purchase order now has two approval workflows. While the Procurement manager confirms,

Store Recieves notification, on validation of the recieved goods from inventory, the application searches for the requests order and updates the product lines, then set
the status to the next workflow approval

----------------------------------------------------------
-For Usage Process(Select Request Usage Mode):

The Signal buttons changes - It automatically moves to the respective workflows. Upon release by store, it deducts the quantity of products in the store



-----------------
- Labour Payment Request

- Select the mode of request (Status) e.g Purchase, Material request Ref, Labour Request Ref.
After approval processes, it generates a payment schedule
-----------------
-Payment Schedule

-After the Request for procurement is Created at the final stage of approval, the application generates a Material payment schedule
The workflow also pass through several mode of approval before it generals a payment voucher




-----------------
- Program of Work

Create a POW for a project -- Enter the Project start Date, Enter Stages, and also the Activities and Schedule Lines.

-Enter the Expected start date for each line.
On computation of POW after confirmation -- The system automatically checks the start date and determines the end date for each line.

The overall duration is now determined by the total number of duration(days) in Activities and Schedule line

You can view them in Gantt view




