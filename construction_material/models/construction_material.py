from odoo import fields, models, api, _
from odoo.exceptions import ValidationError

from datetime import datetime, timedelta
import time
from dateutil.relativedelta import relativedelta

from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.float_utils import float_is_zero, float_compare
from odoo.tools.misc import formatLang
from odoo.addons.base.res.res_partner import WARNING_MESSAGE, WARNING_HELP
import odoo.addons.decimal_precision as dp


class Material_Request(models.Model):
    _name="construction.material"

    _description = "BoQ"


#On selection of branch, opens the stages in branch, the activities appends the ids in selected stage and gets the total of material
#and labour / Appends the material and labour
    name = fields.Char('Name', default='New',)
    ####project_name = fields.Many2one('analytic.account')
    branch_id = fields.Many2one('res.branch',string="Branch")
    project_name = fields.Many2one('project.project','Project')
    house_type = fields.Many2one('construction.housetype', string="House Type")
    plot_id = fields.Many2one('unit.masterx', 'Plot')
    block_no =fields.Many2one('block.masterx', related="plot_id.block_no",string='Block')
    stage = fields.Many2many('stage.models', string='Stage')
    activities = fields.Many2many('activity.type.model', string='Activities')

    material_schedule_o2m = fields.One2many('material.schedule','material_conschedule_id',compute='get_listof_activities',string='Material Line')
    labour_schedule_o2m = fields.One2many('labour.schedule','labour_schedule_id',compute='get_listof_activities',string='Labour Line')

    material_total = fields.Float('Material Total- ', compute='get_lines_total')
    labour_total = fields.Float('Labour Total -',compute='get_lines_total')
    date = fields.Datetime('Date',default= fields.Datetime.now())
    total_overall = fields.Float('Grand Total - ', compute='total_mat_lab')
    state = fields.Selection([('draft','Draft'),('sent','Sent'),
    ('approve', 'Approve'),('done','Done'),('cancel','Cancelled')],track_visibility = "onchange", default='draft',string='State')

    @api.depends('material_total','labour_total')
    def total_mat_lab(self):
        for rec in self:
            rec.total_overall = rec.material_total + rec.labour_total

    @api.depends('material_schedule_o2m','labour_schedule_o2m')
    def get_lines_total(self):
        mat_total = 0.0
        lab_total = 0.0
        for rec in self:
            for rex in rec.material_schedule_o2m:
                mat_total += rex.total
            for ret in rec.labour_schedule_o2m:
                lab_total += ret.total

            rec.material_total = mat_total
            rec.labour_total = lab_total


    @api.depends('branch_id','project_name','house_type')
    def domain_for_stage(self):
        appends = []
        domain = {}
        if self.branch_id:
            search_obj = self.env['stage.models'].search\
            (['&',('branch_id','=',self.branch_id.id),('project_name','=',self.project_name.id),\
            ('house_type','=',self.house_type.id)])
            for rec in search_obj:
                appends.apppend(rec.id)
                domain = {'stage':[('id','in', appends)]}
            return {'domain':domain}

    @api.depends('stage')
    def get_listof_activities(self):
        for rem in self:
            act_append=[]
            lab_append=[]
            mat_append=[]
            for rey in rem.stage:
                search_obj = self.env['stage.models'].search([('id','=',rey.id)])
                for rep in search_obj:
                    for rez in rep.activities:
                        act_append.append(rez.id)
                        rem.activities= [(6,0,act_append)]
                    for rek in rep.activities_material_o2m:
                        mat_append.append(rek.id)
                        rem.material_schedule_o2m = [(6,0,mat_append)]
                    for rew in rep.activities_labour_o2m:
                        lab_append.append(rew.id)
                        rem.labour_schedule_o2m = [(6,0,lab_append)]



#tax_ids': [(6, 0, list(done_taxes))]




class Stage_Model(models.Model):
    _name = 'stage.models'
    name = fields.Char('Stage')
    branch_id = fields.Many2one('res.branch',string="Branch")
    project_name = fields.Many2one('project.project','Project')
    activities = fields.Many2many('activity.type.model', string='Activities')
    activities_material_o2m = fields.One2many('material.schedule','material_stage_id',string='Material Line',compute="get_all_mat_and_labour")
    activities_labour_o2m = fields.One2many('labour.schedule','stage_labour_id',string='Labour Line',compute="get_all_mat_and_labour")
    house_type = fields.Many2one('construction.housetype', string="House Type")

    total = fields.Float(string="Total Amount -", compute="get_Amount_Total_new")

    @api.depends('activities')
    def get_Amount_Total_new(self):
        for rec in self:
            total_mat = 0.0
            for rez in rec.activities:
                total_mat += rez.total
            rec.total = total_mat


    '''@api.onchange('activities')
    def domain_for_mat_and_lab(self):
        mat_append=[]
        lab_append=[]
        domain = {}
        search_mat_obj = self.env['material.schedule'].search(['&',('project_name', '=',self.project_name.id),('branch_id', '=',self.branch_id.id)])
        search_lab_obj = self.env['labour.schedule'].search(['&',('project_name', '=',self.project_name.id),('branch_id', '=',self.branch_id.id)])

        for rec in search_mat_obj:
            mat_append.append(rec.id)
        for rex in search_lab_obj:
            lab_append.append(rex.id)

        domain = {'activities_material_o2m':[('id','in', mat_append)],'activities_labour_o2m':[('id','in', lab_append)]}
        return {'domain':domain}'''

    @api.onchange('branch_id','project_name','house_type')
    def domain_for_stage(self):
        appends = []
        domain = {}
        if self.branch_id:

            search_obj = self.env['activity.type.model'].search([('project_name','=',self.project_name.id),('house_type','=',self.house_type.id)])
            for rec in search_obj:
                appends.append(rec.id)
                domain = {'activities':[('id','in', appends)]}
            return {'domain':domain}

    @api.multi
    @api.depends('activities')
    def get_all_mat_and_labour(self):

        #search_obj = self.env['activity.type.model'].search(['&',('project_name', '=',self.project_name.id),('branch_id', '=',self.branch_id.id)])

        if self.project_name:
            for rep in self:
                mat_append=[]
                lab_append=[]
                for rey in rep.activities:
                    search_obj = self.env['activity.type.model'].search(['&',('project_name', '=',rey.project_name.id),('id', '=',rey.id)])
                    for rec in search_obj:
                        for rez in rec.material_schedule_o2m:
                            mat_append.append(rez.id)
                            rep.activities_material_o2m =[(6,0,mat_append)]# [(4, x.id) for x in rez]#[(6,0,mat_append)]

                        for ret in rec.labour_schedule_o2m:
                            lab_append.append(ret.id)
                            rep.activities_labour_o2m = [(6,0,lab_append)]#[(4, x.id) for x in ret]#[(6,0,lab_append)]

# On selection of activities, write all material and labou of the activies type into material and abour line

#On create , enter all products and materials
class Activity_Model(models.Model):
    _name = 'activity.type.model'
    name = fields.Char('Activity')

    branch_id = fields.Many2one('res.branch',string="Branch")
    project_name = fields.Many2one('project.project','Project')
    material_schedule_o2m = fields.Many2many('material.schedule','material_schedule_id',string='Material Line')#,compute="get_all_mat_and_labour")
    labour_schedule_o2m = fields.Many2many('labour.schedule','labour_schedule_id',string='Labour Line')#,compute="get_all_mat_and_labour")
    house_type = fields.Many2one('construction.housetype', string="House Type")
    total = fields.Float(string="Total Amount -", compute="get_Amount_Total_new")

    @api.depends('material_schedule_o2m','labour_schedule_o2m')
    def get_Amount_Total_new(self):
        for rec in self:
            total_mat = 0.0
            total_lab = 0.0
            for rex in rec.material_schedule_o2m:
                total_mat += rex.total

            for rez in rec.labour_schedule_o2m:
                total_lab += rez.total
            rec.total = total_mat + total_lab


    @api.depends('project_name','branch_id')
    def get_all_mat_and_labour(self):
        pass
        '''search_mat_obj = self.env['material.schedule'].search(['&',('project_name', '=',self.project_name.id),('branch_id', '=',self.branch_id.id)])
        search_lab_obj = self.env['labour.schedule'].search(['&',('project_name', '=',self.project_name.id),('branch_id', '=',self.branch_id.id)])

        mat_append=[]
        lab_append=[]
        if self.project_name:
            for rec in search_mat_obj:
                mat_append.append(rec.id)
            for rex in search_lab_obj:
                lab_append.append(rex.id)
            self.material_schedule_o2m = [(6,0,mat_append)]
            self.labour_schedule_o2m = [(6,0,lab_append)]'''
    @api.onchange('branch_id','project_name')
    def domain_for_mat_and_lab(self):
        mat_append=[]
        lab_append=[]
        domain = {}
        search_mat_obj = self.env['material.schedule'].search(['&',('project_name', '=',self.project_name.id),('branch_id', '=',self.branch_id.id)])
        search_lab_obj = self.env['labour.schedule'].search(['&',('project_name', '=',self.project_name.id),('branch_id', '=',self.branch_id.id)])

        #search_mat_obj = self.env['material.schedule'].search(['&',('project_name', '=',self.project_name.id),('branch_id', '=',self.branch_id.id),'&',('house_type','=',self.house_type.id)])
        #search_lab_obj = self.env['labour.schedule'].search(['&',('project_name', '=',self.project_name.id),('branch_id', '=',self.branch_id.id),'&',('house_type','=',self.house_type.id)])

        for rec in search_mat_obj:
            mat_append.append(rec.id)
        for rex in search_lab_obj:
            lab_append.append(rex.id)

        domain = {'material_schedule_o2m':[('id','=', mat_append)],'labour_schedule_o2m':[('id','=', lab_append)]}
        return {'domain':domain}

# Selection of project and branch, it displays the details of material and labour created for the branch and project


class Material_Schedule(models.Model):
    _name = 'material.schedule'


    material_conschedule_id = fields.Many2one('construction.material')
    material_stage_id=fields.Many2one('stage.models')
    material_schedule_id = fields.Many2one('activity.type.model')

    project_name = fields.Many2one('project.project','Project')
    branch_id = fields.Many2one('res.branch',string="Branch")
    house_type = fields.Many2one('construction.housetype', string="House Type")

    product_id = fields.Many2one('product.product', string = 'Product')
    qty = fields.Float('Quantity',default=1.0)

    label = fields.Many2one('product.uom','Label')
    rate = fields.Float('Rate',related='product_id.list_price')
    total = fields.Float('Total',compute='get_total')

    @api.depends('qty','rate')
    def get_total(self):
        for rec in self:
            totals = rec.qty * rec.rate
            #rec.total_amount_before_variance = totals
            rec.total = totals

class Labour_Schedule(models.Model):
    _name = 'labour.schedule'
    _rec_name = "name"

    labour_schedule_id = fields.Many2one('construction.material')
    labour_schedule_o2m = fields.Many2one('activity.type.model')
    stage_labour_id =fields.Many2one('stage.models')
    name = fields.Many2one('labour.name' , string='Labour Name')
    project_name = fields.Many2one('project.project','Project')
    branch_id = fields.Many2one('res.branch',string="Branch")
    house_type = fields.Many2one('construction.housetype', string="House Type")

    qty = fields.Float('Quantity',default=0.0)
    label = fields.Many2one('product.uom','Label')
    rate = fields.Float('Rate',default=0.00)
    total = fields.Float('Amount',compute='get_total')

    @api.depends('qty','rate')
    def get_total(self):
        for rec in self:
            totals = rec.qty * rec.rate
            #rec.total_amount_before_variance = totals
            rec.total = totals



######################################################
class Material_Request_Construction(models.Model):
    _name="construction.material.request"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = "Construction Material Request"
    _order = "id desc"

    def _default_employee(self):
        return self.env.context.get('default_employee_id') or self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)

    def default_partner_id(self):
        partner = self.env['res.partner'].browse([0])
        return partner.id

    READONLY_STATES = {
        'purchase': [('readonly', True)],
        'done': [('readonly', True)],
        'cancel': [('readonly', True)],
    }

    name = fields.Char('Order Reference', required=True, index=True, copy=False, default='New')
    partner_ref = fields.Char('Vendor Reference', copy=False)
    origin = fields.Char('Source Document', copy=False)
    currency_id = fields.Many2one('res.currency', 'Currency', required=True, states=READONLY_STATES,\
        default=lambda self: self.env.user.company_id.currency_id.id)

    order_line = fields.One2many('construction.material.line', 'order_id', string='Request Lines', states={'cancel': [('readonly', True)], 'done': [('readonly', True)]}, copy=True)

    user_id = fields.Many2one('res.users', string="Users", default=lambda a: a.env.user.id)
    boq_id = fields.Many2one('construction.material', string="Bill of Quantity")
    budget_id = fields.Many2one('construction.budget', string="Based on Budget")
    house_type = fields.Many2one('construction.housetype', string="House Type")

    date_order = fields.Datetime('Order Date', required=True, states=READONLY_STATES, index=True, copy=False, default=fields.Datetime.now)

    date_planned = fields.Datetime(string='Overall Deadline', store=True, index=True)

    project_name = fields.Many2one('project.project','Project')
    branch_id = fields.Many2one('res.branch',string="Branch")
    company_id = fields.Many2one('res.company', 'Company', required=True, index=True, states=READONLY_STATES, default=lambda self: self.env.user.company_id.id)
    state = fields.Selection([
        ('draft', 'Supervisor'),
        ('awaiting', 'Awaiting Procurement'),
        ('super2', 'Supervise'),

        ('sent', 'Project Manager'),
        ('to approve', 'Site Manager'),
        ('purchase', 'Purchase'),
        ('store', 'Store Check'),
        ('store2', 'Store Check'),
        ('sm2', 'Site Manager'),
        ('done', 'Store'),('release', 'Released'),
        ('sm3', 'Site Manager'),

        ('audit', 'Audit'),
        ('procurement', 'Awaiting Procurement'),

        ('project_manager2', 'Project Director'),
        ('schedule', 'Schedule'),



        ('cancel', 'Cancelled')
        ], string='Status', readonly=True, index=True, copy=False, default='draft', compute="change_status",store=True,track_visibility='always')

    @api.multi
    def auto_create_schedule(self):
        schedule = self.env['payment.schedule']
        purchase = self.env['purchase.order'].search([('name','ilike',self.origin)])
        t = 0.0
        p = 0.0
        for line in self.order_line:
            t += line.qty
            p += line.rate

        for rec in self:
            vals = {}
            for pur in purchase:
                vals['pay_amount'] = t * p
                vals['date_sche'] = fields.Date.today()
                vals['name'] = self.env.user.partner_id.id
                vals['select_mode'] = "pur"
                vals['purchase_id'] = pur.id
                vals['product_qty'] = t
            rec.write({'state':'schedule'})

            schedule.create(vals)


    employee_id = fields.Many2one('hr.employee', string = 'Employee', default =_default_employee)
    partner_id = fields.Many2one('res.partner', string='Vendor', default = default_partner_id,track_visibility='always')
    notes = fields.Text('Terms and Conditions')


    mode = fields.Selection([
        ('reque', 'Request Usage'),
        ('procure', 'Request Procurement')
        ], string='Mode', readonly=False, index=True, track_visibility='onchange', store=True)

    @api.depends('mode')
    def change_status(self):
        for rec in self:
            if rec.mode == "procure":
                rec.state = "super2"
            elif rec.mode == "reque":
                rec.state = "draft"
                #rec.write({'state':'super2'})

    def check_budget(self):
        for rec in self:
            budget_total = 0.0
            amount_total = 0.0
            for rex in rec.order_line:
                amount_total += rex.total
            get_budget = self.env['construction.budget'].search([('id','=',self.budget_id.id)])
            for bud in get_budget:
                #budget_total = bud.estimated_total
                for bud_boq in bud.boq_lines:
                    for bux in bud_boq:
                        if rec.boq_id.id == bux.id:
                            budget_total = bud.estimated_total
                            if budget_total < amount_total:
                                raise ValidationError('The Total Amount of Requested Capital is Greater than the Budget total.<P/>Kindly remove some items')



    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('construction.material.request') or '/'
        return super(Material_Request_Construction, self).create(vals)


    @api.multi
    def unlink(self):
        for order in self:
            if not order.state == 'cancel':
                raise ValidationError(_('In order to delete a Request order, you must cancel it first.'))
        return super(Material_Request_Construction, self).unlink()
    @api.multi
    def buttonsend_audit(self):
        for rec in self:
            rec.write({'state':'audit'})

    @api.multi
    def buttonsend_audit_project2(self):
        for rec in self:
            rec.write({'state':'project_manager2'})

    @api.multi
    def button_supervisor_to_store(self):
        self.write({'state':'store2'})
    @api.multi
    def button_store_toSM(self):
        self.write({'state':'sm2'})

    @api.multi
    def button_SM_toProcure(self):
        self.write({'state':'awaiting'})
        self.button_procurer()

    @api.multi
    @api.depends('order_line')
    def button_confirm(self): # PM SENDS
        for order in self:
            email_from = order.env.user.email
            group_user_id = self.env.ref('internal_memo_test.group_memo_xx_admin').id
            order.mail_sending(email_from,group_user_id)
            count_avail = [appr_id.state for appr_id in order.order_line if appr_id.state == 'awaiting']
            count_qty = [appr_id.actual_qty for appr_id in order.order_line if appr_id.actual_qty <= 0]
            if count_avail and count_qty:

                order.state = "super2"
                order.mode = "procure"

            else:
                order.write({'state': 'sent'})
        return True

    @api.multi
    @api.depends('order_line')
    def re_button_confirm(self): # PM SENDS
        for order in self:
            email_from = order.env.user.email
            group_user_id = self.env.ref('internal_memo_test.group_memo_xx_admin').id
            order.mail_sending(email_from,group_user_id)
            count_avail = [appr_id.state for appr_id in order.order_line if appr_id.state == 'awaiting']
            count_qty = [appr_id.actual_qty for appr_id in order.order_line if appr_id.actual_qty <= 0]
            if count_avail and count_qty:
                raise ValidationError('Order Line State still in awaiting state')
                order.state = "super2"
                order.mode = "procure"
            else:
                order.write({'state': 'sent'})
        return True


    @api.multi
    def button_approve(self, force=False): # GM APPROVES TO QS

        self.check_budget()
        email_from = self.env.user.email
        group_user_id = self.env.ref('internal_memo_test.group_memo_xx_coo').id

        self.mail_sending(email_from,group_user_id)
        self.write({'state': 'to approve'})

    @api.multi
    def button_procurer(self, force=False): # GM APPROVES TO QS
        for rex in self:
            #count_avail = [appr_id.state for appr_id in rex.order_line if appr_id.procure == True]
            for rem in rex.order_line:
                if rem.procure == True:
                    rem.procure_btn()
                    rem.state = "awaiting"
                else:
                    rem.state = "available"
                    rex.state = "super2"

    @api.multi
    def button_procurer2(self, force=False): # GM APPROVES TO QS
        for rex in self:
            for rem in rex.order_line:
                rem.procure_btn()
            rex.state = "procurement"

    def Quantity_Moves(self):
        for rec in self:
            diff=0.0
            product_boq = []
            product_order=[]
            for reg in rec.order_line:
                for rex in reg:
                    product_boq.append(rex)
                    for tet in product_boq:
                        stock_location = self.env['stock.location']
                        search_location = stock_location.search([('branch_id','=',tet.branch_id.id)])
                        for r in search_location:
                            stock_quant = self.env['stock.quant']
                            search_quanty = stock_quant.search(['&',('location_id','=',r.id),('product_id','=',tet.product_id.id)])
                            if search_quanty:
                                for rey in search_quanty:
                                    #product_boq.append(rey) product_order.append(rex)
                                    if rey.qty >= tet.qty:
                                        diff = rey.qty - tet.qty
                                        rey.write({'qty':diff})
                                        remain = tet.actual_qty - rey.qty
                                        reg.write({'remaining_qty':remain})


    @api.multi
    def button_site_manager_to_store(self, force=False): # QS APPROVES to HOU
        email_from = self.env.user.email
        group_user_id = self.env.ref('internal_memo_test.group_memo_xx_manager').id
        self.mail_sending(email_from,group_user_id)
        self.write({'state': 'done'})
    @api.multi
    def button_confirm_store(self): #HOU
        self.state = 'release'
        self.Quantity_Moves()
        email_from = self.env.user.email
        group_user_id = self.env.ref('purchase.group_purchase_manager').id
        self.mail_sending(email_from,group_user_id)

        '''purchase_obj = self.env['purchase.order'].create({'partner_id': self.partner_id.id,'date_order':self.date_order,\
        'date_planned':self.date_planned or fields.Datetime.now(),'branch_id':self.branch_id.id})

        self.purchase_obj = purchase_obj
        pol = self.env['purchase.order.line']
        for order in self:
            for order1 in order.order_line:
                values = {
                            'order_id':purchase_obj.id,
                            'name':order1.name,
                            'date_planned':order1.date_planned,
                            'product_id':order1.product_id.id,
                            'product_uom':order1.label.id,
                            'product_qty':order1.qty,
                            'price_unit':order1.rate,
                            #'price_tax':order1.price_tax,
                            'price_subtotal':order1.total,
                            'price_total':order1.total,


                            }
                poc = pol.create(values)
                #return poc'''
    @api.multi
    def button_draft(self):
        self.write({'state': 'draft'})
        return {}

    @api.multi
    def button_cancel(self):
        for order in self:
            if order.state not in ['sent', 'purchase', 'to approve','done']:
                order.write({'state': 'cancel'})
            else:
                raise ValidationError('You can cancel a request a confirmed request')

    def mail_sending(self, email_from,group_user_id):

        from_browse =self.env.user.name
        groups = self.env['res.groups']
        for order in self:

            group_users = groups.search([('id','=',group_user_id)])
            group_emails = group_users.users
            email_1 = group_users.users[0]
            email_to = email_1.login


            append_mails = []
            for group_em in group_emails:
                append_mails.append(group_em.login)

            email_froms = str(from_browse) + " <"+str(email_from)+">"
            mail_appends = (', '.join(str(item)for item in append_mails))
            subject = "Procurement Request"
            bodyx = "Dear Sir/Madam, </br>We wish to notify you that a request from {} has been sent to you for approval </br> </br>Kindly review it. </br> </br>Thanks".format(self.employee_id.name)

            mail_data={
                'email_from': email_froms,
                'subject':subject,
                'email_to':email_to,
                'email_cc':mail_appends,
                'reply_to': email_from,
                'body_html':bodyx
                }
            mail_id =  order.env['mail.mail'].create(mail_data)
            order.env['mail.mail'].send(mail_id)


    @api.multi
    def qs_analyze_boq(self):
        search_boq = self.env['construction.material'].search(['&',('project_name', '=',self.project_name.id),('branch_id', '=',self.branch_id.id),('house_type','=',self.house_type.id)])
        product_boq = []
        product_order=[]
        unit_price = 0.0
        for rec in search_boq:
            for record in rec.material_schedule_o2m:
                for res in record:
                    product_boq.append(res)
                for reg in self.order_line:
                    for rex in reg:
                        product_order.append(rex)
            for r in product_boq:
                for y in product_order:

                    if r.product_id.id == y.product_id.id:
                        unit_price = r.rate
                        reg.write({'actual_price': unit_price})

                    else:
                        reg.write({'actual_price': y.product_id.list_price})




    '''@api.multi
    def qs_analyze_boq(self):
        search_boq = self.env['construction.material'].search(['&',('project_name', '=',self.project_name.id),('branch_id', '=',self.branch_id.id),('house_type','=',self.house_type.id)])
        for ret in search_boq:
            const_mat = [product.product_id.id for product in ret.material_schedule_o2m]
            order = [x for x in self.order_line]
            order_prod = [product.product_id.id for product in self.order_line]
            for pro in const_mat:
                for ord_prod in order_prod:
                    if pro == order_prod:
                        order.write({'actual_price':455})'''


    '''@api.multi
    def warehouse_confirm_qty(self):
        """Forward the available quantity to warehouse officer."""
        # Check for any zero value in the qty and raise an exception
        qty_per_line = [request.qty for request in self.request_ids]
        for qty in qty_per_line:
            if qty <= 0:
                raise UserError("Request lines contain zero quantities. Please raise a purchase order in that regard!")
                break
            else:
                raise Warning("The requested items are available. Kindly proceed with the request")'''


class ConstructionHouseType(models.Model):
    _name = 'construction.housetype' #construction.housetype
    name = fields.Char(string='House Type', required=True)
    units = fields.Integer('Units')

class ConstructionMaterial_Line(models.Model):
    _name = 'construction.material.line'
    _description = 'Material Request Line'
    _order = 'id desc'

    STATE = [
        ('not_available', 'Not Available'),
        ('partially_available', 'Partially Available'),
        ('available', 'Available'),
        ('awaiting', 'Awaiting'),
    ]
    def change_uom(self):
        uom = self.env['product.uom'].search([('name','=','Unit(s)')])
        return uom.id

    def project_name_compute(self):
        project = self.order_id.project_name
        return project
    def branch_name_compute(self):
        branch = self.order_id.branch_id
        return branch

    order_id = fields.Many2one('construction.material.request', string='Reference', index=True, required=True, ondelete='cascade')
    name = fields.Text(string='Description', required=True)

    product_id = fields.Many2one('product.product', string='Product', domain=[('purchase_ok', '=', True)], change_default=True, required=True)
    label = fields.Many2one('product.uom', string='UOM', default=change_uom, required=True)
    qty = fields.Float('Requested Qty',default=1.0, )
    rate = fields.Float('Rate')#,related='product_id.list_price')
    total = fields.Float('Total',compute='get_total')
    date_planned = fields.Datetime(string='Exp. Date', default=fields.Datetime.now(),required=True, index=True)
    remaining_qty = fields.Float('Remaining Qty')
    actual_price = fields.Float('Actual Price')
    project_name = fields.Many2one('project.project','Project', default=project_name_compute)
    branch_id = fields.Many2one('res.branch',string="Branch", default=lambda self:self.env.user.branch_id.id)
    actual_qty= fields.Float('Stock Qty',compute="Check_Product_Availability",store=False)
    stage = fields.Many2many('stage.models', string='Stage')
    activities = fields.Many2many('activity.type.model', string='Activities')
    procure = fields.Boolean('Procure', default=False)
    #move_ids = fields.One2many('stock.move', 'purchase_line_id', string='Reservation', readonly=True, ondelete='set null', copy=False)
    purchase_order_id = fields.Many2one(comodel_name="purchase.order", string='Purchase Order')
    state = fields.Selection(selection=STATE, string='State', compute='_compute_state', store=False)

    @api.depends('qty','actual_price')
    def get_total(self):
        for rec in self:
            totals = rec.qty * rec.actual_price
            rec.total = totals
    @api.depends('branch_id','project_name')
    def domain_for_stage(self):
        appends = []
        domain = {}
        if self.branch_id:
            search_obj = self.env['stage.models'].search(['&',('branch_id','=',self.branch_id.id),('project_name','=',self.project_name.id)])
            for rec in search_obj:
                appends.apppend(rec.id)
                domain = {'stage':[('id','in', appends)]}
            return {'domain':domain}

    @api.depends('stage')
    def domain_for_activities(self):
        appends = []
        domain = {}
        if self.stage:
            for rem in self:
                for rel in rem.stage:
                    for tet in rel.activities:
                    #search_obj = self.env['stage.models'].search([('id','in',rem.branch_id.id),('project_name','=',self.project_name.id)])
            #for rec in search_obj:
                        appends.apppend(tet.id)
                domain = {'activities':[('id','in', appends)]}
            return {'domain':domain}
    @api.depends('stage')
    def Domain_Product_Filter(self):
        appends = []
        domain = {}
        if self.activities:
            for rem in self:
                for rel in rem.activities:
                    for tet in rel.material_schedule_o2m:
                    #search_obj = self.env['stage.models'].search([('id','in',rem.branch_id.id),('project_name','=',self.project_name.id)])
            #for rec in search_obj:
                        appends.apppend(tet.product_id.id)
                domain = {'product_id':[('id','=', appends)]}
            return {'domain':domain}

    @api.depends('product_id')
    def Check_Product_Availability(self):
        stock_location = self.env['stock.location']
        search_location = stock_location.search([('branch_id','=',self.branch_id.id)])
        for r in search_location:#stock_location.browse(search_location):
            total = 0.0
            diff=0.0
            #for rec in r:
            stock_quant = self.env['stock.quant']
            search_quant = stock_quant.search([('location_id','=',r.id),('product_id','=',self.product_id.id)])
            for ret in search_quant:
                    #.browse(search_quant):
                    #if total < 0:
                        #raise ValidationError("There is no Available Quantity in the Location (Warehouse)")
                total += ret.qty
                self.actual_qty = total


        ###################################### - - - - - - - - - ########################################
    @api.depends('actual_qty')

    def _compute_state(self):
        if self.actual_qty <= 0:
            self.state = 'awaiting'
            self.procure = True
        else:
            self.procure = False
            self.state = 'available'

    @api.onchange('order_id')
    @api.one
    def change_record(self):
        if self.order_id.mode == "procure":
            self.write({'procure':True})


    @api.multi
    def procure_btn(self, context=None):
        """
        Method to procure products when they become unavailable in the warehouse
        :param context: Context supplies the needed values including: partner_id, warehouse_id, location_id
        :return:
        """
        purchase_obj = self.env['purchase.order']
        """
        To get the picking type i need to apply a domain where:
             1. warehouse_id = the warehouse_id on the IR,
             2. default_location_dest_id = src_location_id on the IR
        """

        warehouse = self.env['stock.warehouse'].search([('branch_id','=',self.branch_id.id)])

        location = self.env['stock.location'].search([('branch_id','=',self.branch_id.id)])

        warehouse_id = []
        location_id = []
        for i in warehouse:
            warehouse_id.append(i.id)
        for y in location:
            location_id.append(y.id)


        if not (warehouse and location):
            raise ValidationError("Selected Branch must be added to an Warehouse and Location")
        for reck in warehouse_id:
            for tec in location_id:
                domain = [
                    ('code', '=', 'incoming'),
                    ('warehouse_id', '=', reck),
                    ('active', '=', True),
                    ('default_location_dest_id', '=', tec)
                ]

                picking_type_id = self.env['stock.picking.type'].search(domain)
                picking_type_id2 = self.env['stock.picking.type'].search([('active', '=', True)])[0]
                partner_obj = self.env['res.partner']
                partner_id = partner_obj.search([('name', '=', '/')]) and partner_obj.search([('name', '=', '/')]).id or \
                partner_obj.create({'name': '/', 'supplier': True}).id


                values = {

                    'partner_id': partner_id,  # compulsory
                    'date_order': time.strftime("%m/%d/%Y %H:%M:%S"),  # compulsory
                    'picking_type_id': picking_type_id2.id,  # compulsory
                    'date_planned': time.strftime("%m/%d/%Y %H:%M:%S"),  # compulsory str
                    'branch_id':self.branch_id.id,
                    'order_line': [
                        (0, 0, {
                            'order_id':purchase_obj.id,
                            #'request_ref':self.id,
                            'product_id': self.product_id.id,
                            'name':self.name,
                            'product_qty': self.qty,
                            'price_unit': self.product_id.list_price,
                            'product_uom': self.label.id,
                            'name': self.product_id.name,
                            'date_planned': self.date_planned,#time.strftime("%m/%d/%Y %H:%M:%S"),
                            'price_subtotal':self.product_id.list_price * self.qty,
                            'price_total':self.product_id.list_price * self.qty,

                        })
                    ]  # compulsory fields: product_id, product_qty, price_unit, date_planned
                }
                purchase_order_id = purchase_obj.create(values)
                return self.write({'purchase_order_id': purchase_order_id.id,'state':'not_available'})

    @api.multi
    def procure_btn2(self, context=None):
        """
        Method to procure products when they become unavailable in the warehouse
        :param context: Context supplies the needed values including: partner_id, warehouse_id, location_id
        :return:
        """
        purchase_obj = self.env['purchase.order']
        """
        To get the picking type i need to apply a domain where:
             1. warehouse_id = the warehouse_id on the IR,
             2. default_location_dest_id = src_location_id on the IR
        """

        warehouse = self.env['stock.warehouse'].search([('branch_id','=',self.branch_id.id)])

        location = self.env['stock.location'].search([('branch_id','=',self.branch_id.id)])

        warehouse_id = []
        location_id = []
        for i in warehouse:
            warehouse_id.append(i.id)
        for y in location:
            location_id.append(y.id)

        if not (warehouse and location):
            raise ValidationError("Selected Branch must be added to an Warehouse and Location")
        for reck in warehouse_id:
            for tec in location_id:
                domain = [
                    ('code', '=', 'incoming'),
                    ('warehouse_id', '=', reck),
                    ('active', '=', True),
                    ('default_location_dest_id', '=', tec)
                ]

                picking_type_id = self.env['stock.picking.type'].search(domain)
                picking_type_id2 = self.env['stock.picking.type'].search([('active', '=', True)])[0]
                partner_obj = self.env['res.partner']
                partner_id = partner_obj.search([('name', '=', '/')]) and partner_obj.search([('name', '=', '/')]).id or \
                partner_obj.create({'name': '/', 'supplier': True}).id

                values = {

                    'partner_id': partner_id,  # compulsory
                    'date_order': time.strftime("%m/%d/%Y %H:%M:%S"),  # compulsory
                    'picking_type_id': picking_type_id2.id,  # compulsory
                    'date_planned': time.strftime("%m/%d/%Y %H:%M:%S"),  # compulsory str
                    'branch_id':self.branch_id.id,
                    'order_line': [
                        (0, 0, {
                            'order_id':purchase_obj.id,
                            #'request_ref':self.id,
                            'product_id': self.product_id.id,
                            'name':self.name,
                            'product_qty': self.qty,
                            'price_unit': self.product_id.list_price,
                            'product_uom': self.label.id,
                            'name': self.product_id.name,
                            'date_planned': self.date_planned,#time.strftime("%m/%d/%Y %H:%M:%S"),
                            'price_subtotal':self.product_id.list_price * self.qty,
                            'price_total':self.product_id.list_price * self.qty,

                        })
                    ]  # compulsory fields: product_id, product_qty, price_unit, date_planned
                }
                purchase_order_id = purchase_obj.create(values)

                return self.write({'purchase_order_id': purchase_order_id.id,'state':'not_available'})


class Picking(models.Model):
    _inherit= "stock.picking"

    @api.multi
    def do_new_transfer(self):

        res = super(Picking, self).do_new_transfer()

        cons_req = []
        stock_line = []
        po_obj =self.env['purchase.order'].search([('name','ilike',self.origin)])


        '''if po_obj:
            cos = self.env['construction.material.line'].search([('purchase_order_id','=',po_obj.id)])
            construction_request = self.env['construction.material.request'].search([('id','=',cos.order_id.id)])
            if cos and construction_request:

                for request in cos:
                    cons_req.append(request)
                for stpick in self.pack_operation_product_ids:
                    stock_line.append(stpick)
                for x in cons_req:
                    for y in stock_line:
                        if x.product_id.id == y.product_id.id:
                            cos.write({'actual_qty': y.qty_done,'state':'available'})
                            construction_request.write({'state':'sm3','origin':self.origin})
                        else:
                            raise ValidationError('No record to found to write the quantity <br/> in Material Request line')
            else:
                raise ValidationError('No record found for the procurement in PO')
        else:
            pass
           # raise ValidationError('None found')'''

        return res






######## select all boqs, total sum alls grand total, on request, add button to select overal, by project, by time etc

class Construction_budget(models.Model):
    _name = "construction.budget"

    _description = "Construction Budget"
    _order = "id desc"

    def _default_employee(self):
        return self.env.context.get('default_employee_id') or self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)

    name = fields.Char('Description', required=True, index=True, copy=False, default='Description')
    master_build_line_id = fields.Many2one('master.budget2', 'Master Budget', )
    master_bud = fields.Many2one('master.budget2', 'Master Budget', )
    user_id = fields.Many2one('res.users', string="Users", default=lambda a: a.env.user.id)
    boq_lines = fields.Many2many('construction.material', string="Bill of Quantity")
    project_name = fields.Many2one('project.project','Project')
    branch_id = fields.Many2one('res.branch',string="Branch")
    house_type = fields.Many2one('construction.housetype', string="House Type")
    plot_id = fields.Many2one('unit.masterx', 'Plot')
    block_no =fields.Many2one('block.masterx', related="plot_id.block_no",string='Block')
    date_order = fields.Datetime('Date', required=True,index=True, copy=False, default=fields.Datetime.now())

    date_from = fields.Datetime(string='Period', store=True, index=True)
    date_to = fields.Datetime(string='To', store=True, index=True)

    budget_amount_after = fields.Float('Budget Amount After Variance', required=True,store=True, digits=0)
    budget_amount_before = fields.Float('Budget Amount Before Variance', required=True, default=0.0)
    released_amount = fields.Float('Released Amount', required=True, digits=0)
    balance_amount = fields.Float(string='Balance Amount', default=0.0, compute='_compute_balance_amount' )

    state = fields.Selection([
        ('draft', 'Draft'),
        ('approve', 'First Approval'),
        ('validate', 'Validated'),
        ('cancel', 'Cancelled')
        ], string='Status', readonly=True, index=True, copy=False, default='draft', track_visibility='onchange')

    employee_id = fields.Many2one('hr.employee', string = 'Employee', default =_default_employee)
    notes = fields.Text('Terms and Conditions')
    estimated_total = fields.Float('Estimated Amount', compute="get_boq_grandtotal",store=True)

    mode = fields.Selection([
        ('branch', 'By Branch'),
        ('project', 'By Project'),
        ('plot', 'By Plot'),
        ('overall', 'All')
        ], string='Mode', readonly=False, index=True, default='overall', track_visibility='onchange')
    @api.multi
    def button_draft(self):
        self.write({'state': 'draft'})
        return {}



    @api.multi
    def button_approve1(self):
        for gec in self:
            main = []
            gec.state = "approve"

            '''get_master = self.env['master.budget2'].search([('id','=',gec.master_bud.id)])
            for rec in get_master:
                main.append(gec.id)
                get_master.write({'buildmain_budget_lines': [(6,0,main)]})'''



    '''@api.multi
    def button_approve1(self):
        for rem in self:
            act_append=[]
            get_master = self.env['master.budget2'].search([('id','=',gec.master_bud.id)])
            for rey in get_master:
                #search_obj = self.env['stage.models'].search([('id','=',rey.id)])
                for rep in rey:
                    for rez in rep.activities:
                        #act_append.append(rez.id)
                        act_append.append((0,0,{'activities':rez.id}))
                        rem.act_lines=act_append'''


    @api.multi
    def button_validate(self):
        for gec in self:
            gec.state = "validate"

            main = []
            get_master = self.env['master.budget2'].search([('id','=',gec.master_bud.id)])
            for rec in get_master:
                main.append(gec.id)
                get_master.write({'buildmain_budget_lines': [(6,0,main)]})


    @api.multi
    def button_cancel(self):
        self.write({'state': 'cancel'})
        return {}


    @api.onchange('branch_id','project_name','plot_id')
    def domain_for_mat_and_lab(self):
        bill_append=[]
        bill_append2=[]
        domain = {}
        if self.mode == "overall":
            '''search_mat_obj = self.env['construction.material'].search(['&',('project_name', '=',self.project_name.id),('branch_id', '=',self.branch_id.id)])
            #search_mat_obj = ['&', '&', ('project_name', '=',self.project_name.id), ('branch_id', '=',self.branch_id.id), ('plot_id','n', self.plot_id)]
            for rec in search_mat_obj:
                bill_append.append(rec.id)
            domain = {'boq_lines':[('id','=', bill_append)]}
            else:'''
            domain = {'boq_lines':[]}

        else:
            if self.mode == "plot":
                search_mat_obj2 = ['&', '&', ('project_name', '=',self.project_name.id), ('branch_id', '=',self.branch_id.id), ('plot_id','in', self.plot_id.id)]
                #domain = {'boq_lines':[]}
                for recx in search_mat_obj2:
                    bill_append2.append(recx)
                domain = {'boq_lines':[('id','in', bill_append2)]}
            else:
                if self.mode in ["project","branch"]:
                    search_mat_obj = self.env['construction.material'].search(['&',('project_name', '=',self.project_name.id),('branch_id', '=',self.branch_id.id)])
                    #search_mat_obj = ['&', '&', ('project_name', '=',self.project_name.id), ('branch_id', '=',self.branch_id.id), ('plot_id','n', self.plot_id)]
                    for rec in search_mat_obj:
                        bill_append.append(rec.id)
                    domain = {'boq_lines':[('id','=', bill_append)]}


        return {'domain':domain}


    @api.depends('boq_lines')
    def get_boq_grandtotal(self):
        total = 0.0
        for rec in self.boq_lines:
            total += rec.total_overall

        self.estimated_total= total

    @api.depends('budget_amount_after','estimated_total')
    def _compute_balance_amount(self):
        for rec in self:
            rec.balance_amount = rec.estimated_total - rec.budget_amount_after





class ConstructionProgramWork(models.Model):
    _name = "const.programwork"

    _description = "Program of Work"
    _order = "id desc"

    def _default_employee(self):
        return self.env.context.get('default_employee_id') or self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)

    name = fields.Char('Description', required=True, index=True, copy=False, default='Description')
    user_id = fields.Many2one('res.users', string="Users", default=lambda a: a.env.user.id)
    project_name = fields.Many2one('project.project','Project')
    branch_id = fields.Many2one('res.branch',string="Branch")
    house_type = fields.Many2one('construction.housetype', string="House Type")
    date_order = fields.Datetime('Project Start Date', required=True,index=True, copy=False, default=fields.Datetime.now())
    overall_enddate = fields.Datetime('Project End Date', required=True,index=True, copy=False,compute="compute_enddate")

    act_lines = fields.One2many('program.activity.lines', "program_work_id",string="Activitiies")

    state = fields.Selection([
        ('draft', 'Draft'),
        ('compute','Computed'),
        ('approve', 'First Approval'),
        ('validate', 'Validated'),
        ('cancel', 'Cancelled')
        ], string='Status', readonly=True, index=True, copy=False, default='draft', track_visibility='onchange')

    employee_id = fields.Many2one('hr.employee', string = 'Employee', default =_default_employee)
    notes = fields.Text('Note')
    estimated_duration = fields.Float('Estimated Duration', store=True)#,compute="compute_total_duration")
    stage = fields.Many2many('stage.models',store=True, readonly=False,string='Stage',compute='get_listof_activities')

    '''@api.depends('estimated_duration')'''
    def compute_enddate(self):
        for rec in self:
            chckout = rec.date_order
            diff = 0.0
            for rep in rec.act_lines:
                diff += rep.duration
                rec.estimated_duration = diff
            if chckout:
                server_dt = DEFAULT_SERVER_DATETIME_FORMAT
                order_dt = datetime.strptime(chckout, server_dt)
                diff_increase = order_dt + timedelta(days=int(diff))

                rec.overall_enddate = diff_increase



    @api.depends('stage')
    def get_listof_activities(self):
        for rem in self:
            act_append=[]
            for rey in rem.stage:
                #search_obj = self.env['stage.models'].search([('id','=',rey.id)])
                for rep in rey:
                    for rez in rep.activities:
                        #act_append.append(rez.id)
                        act_append.append((0,0,{'activities':rez.id}))
                        rem.act_lines=act_append

    @api.onchange('date_order')
    def change_date_order(self):
        for rec in self:
            for rem in rec.act_lines:
                rem.write({'context_date':rec.date_order})
    @api.multi
    def compute_work_schedule(self):
        for rec in self:
            for rem in rec.act_lines:
                chckout = rem.start_date
                diff = rem.diff
                if chckout:
                    server_dt = DEFAULT_SERVER_DATETIME_FORMAT
                    start_dt = datetime.strptime(chckout, server_dt)
                    diff_increase = start_dt + timedelta(days=int(diff))
                    rem.end_date = diff_increase
                    self.compute_enddate()
                    self.write({'state':'compute'})

    @api.multi
    def button_draft(self):
        self.write({'state': 'draft'})
        return {}

    @api.multi
    def button_approve1(self):
        self.write({'state': 'approve'})
        return {}
    @api.multi
    def button_validate(self):
        self.write({'state': 'validate'})
        return {}

    @api.multi
    def button_cancel(self):
        self.write({'state': 'cancel'})
        return {}
class Construction_Program_Activities(models.Model):
    _name = "program.activity.lines"

    _description = "Activities Schedule"
    _order = "id desc"

    @api.model
    def _get_context_date(self):
        if 'date' in self._context:
            return self._context['date']
        return time.strftime(DEFAULT_SERVER_DATETIME_FORMAT)

    name = fields.Char('Description', required=False, index=True, copy=False, default='Description')
    program_work_id = fields.Many2one('const.programwork','Project')
    activities = fields.Many2one('activity.type.model', string='Activities')

    context_date = fields.Datetime(string='Project StartDate', default=_get_context_date, index=True)
    start_date = fields.Datetime(string='Start Date', store=True, index=True)
    end_date = fields.Datetime(string='End Date', store=True, index=True,compute='change_date_extension')
    diff = fields.Float('Back Difference', store=True,compute='change_date_extension')
    duration = fields.Float('Estimated Duration', store=True,compute='change_main_duration')
    predecessor = fields.Float('Predecessor')#, store=True,compute='change_main_duration')

#
    @api.depends('context_date','start_date')
    def change_date_extension(self):
        difference = 0.0
        diff_increase = 0
        for rec in self:
            chckin = rec.context_date
            chckout = rec.start_date
            if chckin and chckout:
                server_dt = DEFAULT_SERVER_DATETIME_FORMAT
                context_dt = datetime.strptime(chckin, server_dt)
                start_dt = datetime.strptime(chckout, server_dt)
                dur = start_dt - context_dt
                rec.diff = dur.days

                #diff_increase = start_dt + timedelta(days=int(difference))
                #rec.end_date = diff_increase


    @api.depends('end_date')
    def change_main_duration(self):
        for rec in self:
            start = rec.start_date
            end = rec.end_date
            if start and end:
                server_dt = DEFAULT_SERVER_DATETIME_FORMAT
                strt = datetime.strptime(start,server_dt)
                ends = datetime.strptime(end,server_dt)
                durations = ends - strt
                rec.duration = durations.days







#############################################################
#############################################################
#############################################################
#############################################################
#############################################################

class MasterBudget2(models.Model):
    _name = "master.budget2"
    _description = "Budget"
    _inherit = ['mail.thread']

    @api.model
    def create(self, vals):
        if vals.get('name', 'Budget Name') == 'Budget Name':
            vals['name'] = self.env['ir.sequence'].next_by_code('master.budget2')# or '/'
        return super(MasterBudget2, self).create(vals)

    name = fields.Char('Budget Name', required=False,default='Budget Name')
    creating_user_id = fields.Many2one('res.users', 'Responsible', default=lambda self: self.env.user)
    ####budget_master = fields.Many2one('master.budget', 'Master Budget') ######states={'done': [('readonly', True)]}
    project_name = fields.Many2one('project.project', 'Project Name')
    budget_category = fields.Selection([
        ('master', 'Master'),
        ('sub', 'Sub'),
        ], 'Budget Category', default='master', index=True, required=True, copy=False, track_visibility='always')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('pm', 'Project Manager'),
        ('pd', 'Project Director'),
        ('com', 'Compliance'),
        ('bod', 'Board'),
        ('cancel', 'Cancelled'),
        ('confirm', 'Confirmed'),
        ('validate', 'Validated'),
        ('done', 'Done')
        ], 'Status', default='draft', index=True, required=True, readonly=True, copy=False, track_visibility='always')
    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic Account')
    budget_amount_after = fields.Float('Budget Amount After Variance', required=True,store=True, digits=0)
    budget_amount_before = fields.Float('Budget Amount Before Variance', required=True, digits=0,compute='get_general_budget_total')
    released_amount = fields.Float('Released Amount', required=True, digits=0)
    balance_amount = fields.Float(string='Balance Amount', default=0.0, compute='_compute_balance_amount' )
    approve_date = fields.Date('Approve Date', default = fields.Date.today())

    date_from = fields.Date('Start Date', required=True)
    date_to = fields.Date('End Date', required=True)
    released_amount_date = fields.Date('Paid Date')
    purpose = fields.Text('Purpose', required=True)
    percentage = fields.Float(compute='_compute_percentage', string='Budget performance(%)',size=6)

    '''buildmain_budget_lines = fields.One2many('construction.budget','master_build_line_id',string='Builidng',domain=[('state','=','validate')])
    infrasture_budget_lines = fields.One2many('infrastructure.budget2','master_build_line_id',string='Infrastructure',domain=[('state','=','validate')])
    salaries_budget_lines = fields.One2many('salary.budget2','master_build_line_id',string='Salaries',domain=[('state','=','validate')])
    opex_budget_lines = fields.One2many('opex.budget2','master_build_line_id',string='Opex', domain=[('state','=','validate')])
    capex_budget_lines = fields.One2many('capex.budget2','master_build_line_id',string='Capex',domain=[('state','=','validate')])
    ancil_budget_lines = fields.One2many('ancil.budget2','master_build_line_id',string='Anciliary')'''


    buildmain_budget_lines = fields.Many2many('construction.budget',string='Builidng',domain=[('state','=','validate')])
    infrasture_budget_lines = fields.Many2many('infrastructure.budget2',string='Infrastructure',domain=[('state','=','validate')])
    salaries_budget_lines = fields.Many2many('salary.budget2',string='Salaries',domain=[('state','=','validate')])
    opex_budget_lines = fields.Many2many('opex.budget2',string='Opex', domain=[('state','=','validate')])
    capex_budget_lines = fields.Many2many('capex.budget2',string='Capex',domain=[('state','=','validate')])
    ancil_budget_lines = fields.Many2many('ancil.budget2',string='Anciliary')


    '''@api.depends('buildmain_budget_lines','infrasture_budget_lines',\
    'salaries_budget_lines','opex_budget_lines','capex_budget_lines','ancil_budget_lines')
    def get_general_budget_total(self):
        total_build = 0.0
        total_infra = 0.0
        total_ancil = 0.0
        total_opexx = 0.0
        total_capex = 0.0
        total_salar = 0.0

        for rec in self:
            for rex1 in rec.buildmain_budget_lines:
                total_build += rex1.budget_amount_before
                for rex2 in rec.infrasture_budget_lines:
                    total_infra +='''




    @api.depends('buildmain_budget_lines.estimated_total','infrasture_budget_lines.total_amount',
                'salaries_budget_lines.total_amount','opex_budget_lines.total_amount','capex_budget_lines.total_amount','ancil_budget_lines.budget_amount_before')
    def get_general_budget_total(self):
        total_build = 0.0
        total_infra = 0.0
        total_ancil = 0.0
        total_opexx = 0.0
        total_capex = 0.0
        total_salar = 0.0

        for rec in self:

            for ret in rec.buildmain_budget_lines:
                total_build += ret.estimated_total
            for ret2 in rec.infrasture_budget_lines:
                total_infra += ret2.total_amount
            for ret3 in rec.salaries_budget_lines:
                total_salar += ret3.total_amount
            for ret4 in rec.opex_budget_lines:
                total_opexx += ret4.total_amount
            for ret5 in rec.capex_budget_lines:
                total_capex += ret5.total_amount
            rec.budget_amount_before = total_build + total_infra + total_ancil + total_opexx + total_capex + total_salar
            #rec.budget_amount_before = sum((total_build,total_infra,total_ancil,total_opexx,total_capex,total_salar))

    @api.multi
    def _compute_percentage(self):
        #line.percentage = float((line.planned_amount or 0.0) / line.practical_amount) * 100
        for line in self:
            if line.budget_amount_before != 0.00:
                line.percentage = float((line.budget_amount_before or 0.0) / line.balance_amount) * 100
            else:
                line.percentage = 0.00

    @api.depends('budget_amount_after','budget_amount_before')
    def _compute_balance_amount(self):
        for rec in self:
            rec.balance_amount = rec.budget_amount_before - rec.budget_amount_after

    @api.multi
    def button_pm_to_pd(self):
        self.write({'state':'pd'})

    @api.multi
    def button_pd_to_com(self):
        self.write({'state':'com'})

    @api.multi
    def button_com_to_bod(self):
        self.write({'state':'bod'})

    @api.multi
    def confirm_budget(self):
        self.write({'state':'confirm'})
    @api.multi
    def cancel_budget(self):
        for line in self:
            line.write({'state':'cancel'})

    @api.multi
    def validate_budget(self):
        for line in self:
            line.state = "validate"
            line.approve_date = fields.Date.today()

    @api.multi
    def set_draft_budget(self):
        for rec in self:
            rec.state = "draft"
    @api.multi
    def unlink(self):
        """
        Overrides orm unlink method.
        @param self: The object pointer
        @return: True/False.
        """
        if self.state in "confirm" or "validate":
            raise ValidationError('You cannot delete a confirm or validated budget')

        return super(MasterBudget2, self).unlink()

class AnciliaryBudget2(models.Model):
    _name = "ancil.budget2"
    _description = "Ancilliary Budget"
    master_build_line_id = fields.Many2one('master.budget2', 'Master Budget', )
    budget_amount_before = fields.Float('Budget Amount Before Variance', default=0.0, digits=0)

    name = fields.Char('Description', required=True)
    master_bud = fields.Many2one('master.budget2', 'Master Budget', )
    department = fields.Many2one('hr.department', 'Department', )
    ancil_line = fields.One2many('opex.capex.line2','ancil_line_id',string="Anciliary")

    total_amount = fields.Float('Total Amount', compute='get_budget_total', digits=0)
    total_after = fields.Float('Total After Variance', compute='get_after_total', digits=0)
    released_amount = fields.Float('Release Amount', digits=0)
    approve_checkin_date = fields.Datetime('Approved Date', required=True, readonly=True, default=lambda *a: (datetime.now() + relativedelta(minutes=1)).strftime('%Y-%m-%d %H:%M:%S'))#
    #checkout_date = fields.Datetime('Check Out', required=True, readonly=True,states={'draft': [('readonly', False)]},default=lambda *a: (datetime.datetime.now() + relativedelta(days=1)).strftime('%Y-%m-%d %H:%M:%S'))#

    state = fields.Selection([
        ('draft', 'Draft'),
        ('cancel', 'Cancelled'),
        ('confirm', 'Confirmed'),
        ('validate', 'Validated'),
        ('done', 'Done')
        ], 'Status', default='draft', index=True, required=True, readonly=True, copy=False, track_visibility='always')

    @api.multi
    def confirm_budget(self):
        self.write({'state':'confirm'})
    @api.multi
    def cancel_budget(self):
        mas_budget_obj = self.env['master.budget2']
        for line in self:
            for rec in line.salaries_budget_lines:
                mas_search = mas_budget_obj.search([('id','=',self.id)])
                if mas_search:
                    mas_search.unlink()

            line.write({'state':'cancel'})

    '''@api.multi
    def validate_budget(self):
        for rec in self:
            id_main = []
            get_budget_id = self.env['master.budget2']#.search([('id','=',self.master_build_line_id.id)])
            #if get_budget_id:
            id_main.append(rec.id)
            rec.master_build_line_id.opex_budget_lines = [(6,0,id_main)]
            rec.state = "validate"'''
    @api.multi
    def validate_budget(self):
        self.write({'state':'validate'})


    @api.depends('ancil_line')
    def get_budget_total(self):
        for rec in self:
            total = 0.0
            for rex in rec.ancil_line:
                total += rex.budget
            rec.total_amount  = total
    @api.depends('released_amount','total_amount')
    def get_after_total(self):
        for rec in self:
            rec.total_after = rec.total_amount - rec.released_amount

    @api.multi
    def set_draft_budget(self):
        self.state = "draft"
    @api.multi
    def unlink(self):
        """
        Overrides orm unlink method.
        @param self: The object pointer
        @return: True/False.
        """
        if self.state in "confirm" or "validate":
            raise ValidationError('You cannot delete a confirm or validated budget')

        return super(AnciliaryBudget2, self).unlink()



class InfrastructureModel2(models.Model):
    _name = "infrastructure.budget2"
    _description = "Infrastructure Budget"

    master_build_line_id = fields.Many2one('master.budget2', 'Master Budget', )
    name = fields.Many2one('construction.material', string='BoQ')

########
    stage = fields.Many2many('stage.models', string='Stage')
    activities = fields.Many2many('activity.type.model', string='Activities', compute="_write_stage_activities", readonly=False)
    material_schedule_o2m = fields.One2many('material.schedule','material_conschedule_id',compute='get_listof_activities',string='Material Line')
    labour_schedule_o2m = fields.One2many('labour.schedule','labour_schedule_id',compute='get_listof_activities',string='Labour Line')
#######

    total_amount = fields.Float('Total Amount', compute='get_all_task_totals', digits=0)
    released_amount = fields.Float('Release Amount', digits=0)
    balance_amount = fields.Float('Balance Amount', digits=0, compute="_get_balance_amount")
    ###***stage_activity_lines = fields.One2many('activity.task.line','infrastru_act_id',string='Task / Activities',store=True)#,compute='get_tasks_and_append')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('cancel', 'Cancelled'),
        ('confirm', 'Confirmed'),
        ('validate', 'Validated'),
        ('done', 'Done')
        ], 'Status', default='draft', index=True, required=True, readonly=True, copy=False, track_visibility='always')

    """creates and appends the activity id under stage_activity_lines with all activities found under stage"""
    @api.depends('released_amount','total_amount')
    def _get_balance_amount(self):
        for rec in self:
            rec.balance_amount = rec.total_amount - rec.released_amount

    @api.onchange('boq')
    def get_domain_boq(self):
        item = []
        domain = {}
        for rec in self:
            for r in rec.boq:
                for rex in r.stage:
                    item.append(rex.id)
            domain = {'stage':[('id', '=', item)]}
        return {'domain':domain}


    @api.depends('stage')
    def _write_stage_activities(self):
        item = []
        for rec in self:
            for record in rec.stage:
                for rex in record.activities:
                    item.append(rex.id)
            rec.activities = [(6,0,item)]

    @api.depends('activities')
    def get_listof_activities(self):
        for rem in self:
            act_append=[]
            lab_append=[]
            mat_append=[]
            for rey in rem.activities:
                for rep in rey.material_schedule_o2m:
                    mat_append.append(rep.id)
                    rem.material_schedule_o2m = [(6,0,mat_append)]
                for rew in rey.labour_schedule_o2m:
                    lab_append.append(rew.id)
                    rem.labour_schedule_o2m = [(6,0,lab_append)]



    '''@api.onchange('stage')
    def get_tasks_and_append(self):
        for rec in self:
            task_comm = []
            total = 0.0
            get_stage_and_act = self.env['bill.quantity'].search([('id','=',rec.boq.id)])
            for level in get_stage_and_act.stage:
                task_id = self.env['bill.of.quantity.stage'].search([('stage_id','=',level.stage_id.id)])

                task_comm.append((0,0,{'activity_task':level.stage_id.id}))
                rec.stage_activity_lines = task_comm'''
    """Gets the total of the calculations done in activity.task.line"""
    @api.depends('material_schedule_o2m','labour_schedule_o2m')
    def get_all_task_totals(self):
        for rec in self:
            total = 0.0
            totallab = 0.0
            for ret in rec.material_schedule_o2m:
                total += ret.total
            for rer in rec.labour_schedule_o2m:
                totallab += rer.total
            total_amount = total + totallab
            rec.total_amount = total_amount

    @api.multi
    def set_draft_budget(self):
        self.state = "draft"
    @api.multi
    def unlink(self):
        """
        Overrides orm unlink method.
        @param self: The object pointer
        @return: True/False.
        """
        #if self.state in "confirm" or "validate":

        if self.state in "confirm":# or "validate":
            raise ValidationError('You cannot delete a confirm budget')

        return super(InfrastructureModel2, self).unlink()
    @api.multi
    def confirm_budget(self):
        self.write({'state':'confirm'})
    @api.multi
    def cancel_budget(self):
        self.write({'state':'cancel'})
        #self.unlink()


    '''@api.multi
    def validate_budget(self):
        for rec in self:
            id_main = []
            get_budget_id = self.env['master.budget2']#.search([('id','=',self.master_build_line_id.id)])
            if get_budget_id:
                #id_main.append((0,0, {'boq':rec.boq,'stage':rec.stage,'total_amount':rec.total_amount,}))
                get_budget_id.infrasture_budget_lines = id_main#(0,0, value)#{id_main})#[(0,0,id_main)]
                get_budget_id.infrasture_budget_lines = [(6,0,[rec.id])]
                rec.state = "validate"'''
    @api.multi
    def validate_budget(self):
        self.write({'state':'validate'})


class ProductInherit(models.Model):
    _inherit = 'product.template'

    capex_period2 = fields.Selection([
        ('2018', '2018'),
        ('2019', '2019'),
        ('2020', '2020'),
        ('2021', '2021'),
        ('2022', '2022'),
        ('2023', '2023'),
        ('2024', '2023'),
        ('2025', '2025'),
        ], 'Period', default='2019', index=True, required=True, readonly=False, copy=False, track_visibility='always')

class OpexModel(models.Model):
    _name = "opex.budget2"
    _description = "OPEX Budget"

    name = fields.Char('Description')
    master_bud = fields.Many2one('master.budget2', 'Master Budget', )
    master_build_line_id = fields.Many2one('master.budget2', 'Master Budget', )
    department = fields.Many2one('hr.department', 'Department', )

    total_amount = fields.Float('Total Amount', compute='get_budget_total', digits=0)
    total_after = fields.Float('Total After Variance', compute='get_after_total', digits=0)
    released_amount = fields.Float('Release Amount', digits=0)
    approve_checkin_date = fields.Datetime('Approved Date', required=True, readonly=True, default=lambda *a: (datetime.now() + relativedelta(minutes=1)).strftime('%Y-%m-%d %H:%M:%S'))#
    #checkout_date = fields.Datetime('Check Out', required=True, readonly=True,states={'draft': [('readonly', False)]},default=lambda *a: (datetime.datetime.now() + relativedelta(days=1)).strftime('%Y-%m-%d %H:%M:%S'))#

    opex_lines = fields.One2many('opex.capex.line2','opex_line_id',string='Opex Activities',store=True, readonly=False)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('cancel', 'Cancelled'),
        ('confirm', 'Confirmed'),
        ('validate', 'Validated'),
        ('done', 'Done')
        ], 'Status', default='draft', index=True, required=True, readonly=True, copy=False, track_visibility='always')

    @api.multi
    def confirm_budget(self):
        self.write({'state':'confirm'})
    @api.multi
    def cancel_budget(self):
        mas_budget_obj = self.env['master.budget2']
        for line in self:
            mas_search = mas_budget_obj.search([('salaries_budget_lines','=',self.id)])
            if mas_search:
                mas_search.unlink()

            line.write({'state':'cancel'})

    '''@api.multi
    def validate_budget(self):
        for rec in self:
            id_main = []
            get_budget_id = self.env['master.budget2']#.search([('id','=',self.master_build_line_id.id)])
            #if get_budget_id:
            id_main.append(rec.id)
            rec.master_build_line_id.opex_budget_lines = [(6,0,id_main)]
            rec.state = "validate"'''
    @api.multi
    def validate_budget(self):
        self.write({'state':'validate'})


    @api.depends('opex_lines.budget')
    def get_budget_total(self):
        for rec in self:
            total = 0.0
            for rex in rec.opex_lines:
                total += rex.budget
            rec.total_amount  = total
    @api.depends('released_amount','total_amount')
    def get_after_total(self):
        for rec in self:
            rec.total_after = rec.total_amount - rec.released_amount

    @api.multi
    def set_draft_budget(self):
        self.state = "draft"
    @api.multi
    def unlink(self):
        """
        Overrides orm unlink method.
        @param self: The object pointer
        @return: True/False.
        """
        if self.state in "confirm" or "validate":
            raise ValidationError('You cannot delete a confirm or validated budget')

        return super(OpexModel2, self).unlink()

class CapexModel2(models.Model):
    _name = "capex.budget2"
    _description = "CAPEX Budget"

    name = fields.Char('Description')
    master_bud = fields.Many2one('master.budget2', 'Master Budget', )
    master_build_line_id = fields.Many2one('master.budget2', 'Master Budget', )
    department = fields.Many2one('hr.department', 'Department', )
    total_amount = fields.Float('Total Amount', compute='get_budget_total', digits=0)
    total_after = fields.Float('Total After Variance', compute='get_after_total', digits=0)
    released_amount = fields.Float('Release Amount', digits=0, compute="_compute_releases_amount")
    approve_checkin_date = fields.Datetime('Approved Date', required=True, readonly=True, default=lambda *a: (datetime.now() + relativedelta(minutes=1)).strftime('%Y-%m-%d %H:%M:%S'))#
    #checkout_date = fields.Datetime('Check Out', required=True, readonly=True,states={'draft': [('readonly', False)]},default=lambda *a: (datetime.datetime.now() + relativedelta(days=1)).strftime('%Y-%m-%d %H:%M:%S'))#

    capex_lines = fields.One2many('opex.capex.line2','capex_line_id',string='Opex Activities',store=True, readonly=False)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('cancel', 'Cancelled'),
        ('confirm', 'Confirmed'),
        ('validate', 'Validated'),
        ('done', 'Done')
        ], 'Status', default='draft', index=True, required=True, readonly=True, copy=False, track_visibility='always')
    @api.depends('capex_lines')
    def _compute_releases_amount(self):
        total = 0.0
        for rec in self:
            for rem in rec.capex_lines:
                total += rem.budget_release
            rec.released_amount = total

    @api.multi
    def confirm_budget(self):
        self.write({'state':'confirm'})
    @api.multi
    def cancel_budget(self):
        mas_budget_obj = self.env['master.budget2']
        for line in self:
            mas_search = mas_budget_obj.search([('salaries_budget_lines','=',self.id)])
            if mas_search:
                mas_search.unlink()

            line.write({'state':'cancel'})

    '''@api.multi
    def validate_budget(self):
        for rec in self:
            id_main = []
            get_budget_id = self.env['master.budget2']#.search([('id','=',self.master_build_line_id.id)])
            if get_budget_id:
                id_main.append(rec.id)
                get_budget_id.capex_budget_lines = [(6,0,id_main)]
                rec.state = "validate"'''
    @api.multi
    def validate_budget(self):
        self.write({'state':'validate'})

    @api.depends('capex_lines.budget')
    def get_budget_total(self):
        for rec in self:
            total = 0.0
            for rex in rec.capex_lines:
                total += rex.budget
            rec.total_amount  = total
    @api.depends('released_amount','total_amount')
    def get_after_total(self):
        for rec in self:
            rec.total_after = rec.total_amount - rec.released_amount
    @api.multi
    def set_draft_budget(self):
        self.state = "draft"
    @api.multi
    def unlink(self):
        """
        Overrides orm unlink method.
        @param self: The object pointer
        @return: True/False.
        """
        if self.state in "confirm" or "validate":
            raise ValidationError('You cannot delete a confirm or validated budget')

        return super(CapexModel2, self).unlink()

class OpexCapexLine2(models.Model):
    _name = "opex.capex.line2"
    description = fields.Text('Payment For')
    month = fields.Selection([
        ('jan', 'January'),
        ('feb', 'Febuary'),
        ('mar', 'March'),
        ('apr', 'April'),
        ('may', 'May'),
        ('jun', 'June'),
        ('jul', 'July'),
        ('aug', 'August'),
        ('sep', 'September'),
        ('oct', 'October'),
        ('nov', 'November'),
        ('dec', 'December'),
        ], 'Month', default='jan', index=True, required=False, readonly=True, copy=False, track_visibility='always')


    period = fields.Selection([
        ('2018', '2018'),
        ('2019', '2019'),
        ('2020', '2020'),
        ('2021', '2021'),
        ('2022', '2022'),
        ('2023', '2023'),
        ('2024', '2023'),
        ('2025', '2025'),
        ], 'Year', default='2019', index=True, required=False, readonly=False, copy=False, track_visibility='always')

    product_id = fields.Many2one('product.template', string = 'Product')

    capex_line_id = fields.Many2one('capex.budget2', string='Capex Id')
    opex_line_id = fields.Many2one('opex.budget2', string='Opex Id')
    ancil_line_id = fields.Many2one('ancil.budget2', string='Anciliary')

    qty = fields.Float('Quantity',default=1.0)
    label = fields.Many2one('product.uom','Unit of Measure')
    rate = fields.Float('Rate',related='product_id.list_price')
    #total_before_variance = fields.Float('Total before Variance',compute='get_total')
    total = fields.Float('Total after Variance',compute='get_diff')
    budget = fields.Float('Budget Amount',compute='get_total')
    budget_release = fields.Float('Released Amount',default=0.0)

    #budget_after_var = fields.Float('Budget After Variance',compute='get_total')


    @api.depends('qty','rate')
    def get_total(self):
        for rec in self:
            totals = rec.qty * rec.rate
            rec.budget = totals
    @api.depends('budget','budget_release')
    def get_diff(self):
        for rec in self:
            rec.total = rec.budget - rec.budget_release

    """On change of period, it filters and appends all the products within the period"""
    @api.onchange('period')
    def filter_domain(self):
        domain = {}
        all_products = []
        for rec in self:
            get_products = self.env['product.template'].search([('capex_period2','=', rec.period)])
            for items in get_products:
                all_products.append(items.id)
            domain = {'product_id':[('id','=',all_products)]}
        return domain


class SalaryBudgetModel2(models.Model):
    _name = "salary.budget2"
    _description = "Salary Budget"

    master_build_line_id = fields.Many2one('master.budget2', 'Master Budget', )
    master_bud = fields.Many2one('master.budget2', 'Master Budget', )
    name = fields.Char('Description')
    department = fields.Many2one('hr.department', 'Department', )
    total_amount = fields.Float('Total Amount', compute='get_budget_total', digits=0)
    released_amount = fields.Float('Release Amount', digits=0)
    difference_budget = fields.Float('Difference Amount', compute='get_difference_total', digits=0)
    approve_checkin_date = fields.Datetime('Approved Date', required=True, readonly=True, default=lambda *a: (datetime.now() + relativedelta(minutes=1)).strftime('%Y-%m-%d %H:%M:%S'))#
    salary_lines = fields.One2many('salary.budget.line2','salary_line_id',string='Salary Lines',store=True, readonly=False)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('cancel', 'Cancelled'),
        ('confirm', 'Confirmed'),
        ('validate', 'Validated'),
        ('done', 'Done')
        ], 'Status', default='draft', index=True, required=True, readonly=True, copy=False, track_visibility='always')

    @api.multi
    def confirm_budget(self):
        self.write({'state':'confirm'})
    @api.multi
    def cancel_budget(self):
        mas_budget_obj = self.env['master.budget2']
        for line in self:
            mas_search = mas_budget_obj.search([('salaries_budget_lines','=',self.id)])
            if mas_search:
                mas_search.unlink()

            line.write({'state':'cancel'})

    '''@api.multi
    def validate_budget(self):
        for rec in self:
            id_main = []
            get_budget_id = self.env['master.budget2']#.search([('id','=',self.master_build_line_id.id)])
            if get_budget_id:
                id_main.append(rec.id)
                get_budget_id.salaries_budget_lines = [(6,0,id_main)]
                rec.state = "validate"'''

    @api.multi
    def validate_budget(self):
        self.write({'state':'validate'})


    @api.depends('total_amount','released_amount')
    def get_difference_total(self):
        diff = 0.0
        for i in self:
            diff = i.total_amount - i.released_amount
        i.difference_budget = diff

    @api.depends('salary_lines')
    def get_budget_total(self):
        for rec in self:
            total = 0.0
            for rex in rec.salary_lines:
                total += rex.budget_total
            rec.total_amount = total

    @api.multi
    def set_draft_budget(self):
        self.state = "draft"

    @api.multi
    def unlink(self):
        """
        Overrides orm unlink method.
        @param self: The object pointer
        @return: True/False.
        """
        if self.state in "confirm" or "validate":
            raise ValidationError('You cannot delete a confirm or validated budget')

        return super(SalaryBudgetModel2, self).unlink()



class SalaryBudgetLine(models.Model):
    _name = "salary.budget.line2"

    months = fields.Selection([
        ('1', '1 Month'),
        ('2', '2 Months'),
        ('3', '3 Months'),
        ('4', '4 Months'),
        ('5', '5 Months'),
        ('6', '6 Months'),
        ('7', '7 Months'),
        ('8', '8 Months'),
        ('9', '9 Months'),
        ('10', '10 Months'),
        ('11', '11 Months'),
        ('12', '12 Months'),
        ], 'Months', default='1', index=True, required=False, readonly=False, copy=False, track_visibility='always')

    employee_id = fields.Many2one('hr.employee', string = 'Employee')
    dept_ids = fields.Char(string ='Department', related='employee_id.department_id.name',readonly = True, store =True)
    job_title = fields.Char(string = 'Job title',related ="employee_id.job_id.name")
    wage = fields.Float('Employee Wage',compute='get_employee_wage', default=0.0)
    budget_total= fields.Float('Budget Amount',compute='get_budget_wage',default=0.0)
    salary_line_id = fields.Many2one('salary.budget2', string='Salary Id')
    duration = fields.Integer('Month Interval',compute='get_interval_wage')

    @api.depends('employee_id')
    def get_employee_wage(self):
        for rec in self:
            wage = 0.0
            get_wage = self.env['hr.contract'].search([('employee_id','=',rec.employee_id.id)])
            rec.wage = get_wage.wage
            #rec.budget_total = rec.wage * rec.duration

    @api.depends('duration','wage')
    def get_budget_wage(self):
        for rec in self:
            rec.budget_total = rec.wage * rec.duration


    @api.depends('months')
    def get_interval_wage(self):
        for rec in self:
            if rec.months == "1":
                rec.duration = 1
            elif rec.months == "2":
                rec.duration = 2

            elif rec.months == "3":
                rec.duration = 3

            elif rec.months == "4":
                rec.duration = 4
            elif rec.months == "5":
                rec.duration = 5
            elif rec.months == "6":
                rec.duration = 6
            elif rec.months == "7":
                rec.duration = 7
            elif rec.months == "8":
                rec.duration = 8
            elif rec.months == "9":
                rec.duration = 9
            elif rec.months == "10":
                rec.duration = 10
            elif rec.months == "11":
                rec.duration = 11
            elif rec.months == "12":
                rec.duration = 12

class labourName(models.Model):
    _name = 'labour.name'

    name = fields.Char('Labour Name')
